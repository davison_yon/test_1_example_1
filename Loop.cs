bool isLooping = true;
int  grossTotal = 0;

while(isLooping == true)
{
    Console.WriteLine("How much money was made?");
    int userInput = int.Parse(Console.ReadLine());
    grossTotal = grossTotal + userInput;
    Console.WriteLine("Gross Total = $" + grossTotal);
    Console.WriteLine();
}
